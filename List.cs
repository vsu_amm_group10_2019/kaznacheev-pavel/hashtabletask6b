﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableTask6b
{
    public class List<T>
    {
        public Node<T> Head { get; set; }
        public Node<T> Tail { get; set; }
        public int size = 0;
        public int Size
        {
            get
            {
                return size;
            }
        }
        public void Add(T film)
        {
            Node<T> node = new Node<T> { Film = film, Next = null };
            if (Head == null)
            {
                Head = node;
            }
            else
            {
                Tail.Next = node;
            }
            Tail = node;
            size++;
        }
        public T Get(int index)
        {
            if (index >= size)
            {
                throw new Exception("Index out of range");
            }
            Node<T> current = Head;
            for (int i = 0; i < index; i++)
            {
                current = current.Next;
            }

            return current.Film;
        }
        public void Delete(int index)
        {
            if (index >= size)
            {
                throw new Exception("Index out of range");
            }

            if (index == 0)
            {
                Head = Head.Next;
                if (size == 1)
                {
                    Tail = null;
                }
                size -= 1;
                return;
            }
            Node<T> current = Head;
            Node<T> previous = null;
            for (int i = 0; i < index; i++)
            {
                previous = current;
                current = current.Next;
            }

            previous.Next = current.Next;
        }
        public void Clear()
        {
            Head = null;
            Tail = null;
            size = 0;
        }
    }
}
