﻿
namespace HashTableTask6b
{
    partial class addFilmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.addTitle = new System.Windows.Forms.TextBox();
            this.addProducer = new System.Windows.Forms.TextBox();
            this.labelProducer = new System.Windows.Forms.Label();
            this.addActors = new System.Windows.Forms.TextBox();
            this.labelActors = new System.Windows.Forms.Label();
            this.addSynopsis = new System.Windows.Forms.TextBox();
            this.labelSynopsis = new System.Windows.Forms.Label();
            this.buttonAddNewFilm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTitle.Location = new System.Drawing.Point(10, 37);
            this.labelTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(73, 19);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "Название";
            // 
            // addTitle
            // 
            this.addTitle.Location = new System.Drawing.Point(103, 37);
            this.addTitle.Margin = new System.Windows.Forms.Padding(2);
            this.addTitle.Name = "addTitle";
            this.addTitle.Size = new System.Drawing.Size(255, 20);
            this.addTitle.TabIndex = 2;
            // 
            // addProducer
            // 
            this.addProducer.Location = new System.Drawing.Point(103, 67);
            this.addProducer.Margin = new System.Windows.Forms.Padding(2);
            this.addProducer.Name = "addProducer";
            this.addProducer.Size = new System.Drawing.Size(255, 20);
            this.addProducer.TabIndex = 4;
            // 
            // labelProducer
            // 
            this.labelProducer.AutoSize = true;
            this.labelProducer.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelProducer.Location = new System.Drawing.Point(10, 67);
            this.labelProducer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelProducer.Name = "labelProducer";
            this.labelProducer.Size = new System.Drawing.Size(74, 19);
            this.labelProducer.TabIndex = 3;
            this.labelProducer.Text = "Режиссёр";
            // 
            // addActors
            // 
            this.addActors.Location = new System.Drawing.Point(103, 96);
            this.addActors.Margin = new System.Windows.Forms.Padding(2);
            this.addActors.Multiline = true;
            this.addActors.Name = "addActors";
            this.addActors.Size = new System.Drawing.Size(255, 51);
            this.addActors.TabIndex = 6;
            // 
            // labelActors
            // 
            this.labelActors.AutoSize = true;
            this.labelActors.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelActors.Location = new System.Drawing.Point(10, 112);
            this.labelActors.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelActors.Name = "labelActors";
            this.labelActors.Size = new System.Drawing.Size(60, 19);
            this.labelActors.TabIndex = 5;
            this.labelActors.Text = "Актёры";
            // 
            // addSynopsis
            // 
            this.addSynopsis.Location = new System.Drawing.Point(103, 151);
            this.addSynopsis.Margin = new System.Windows.Forms.Padding(2);
            this.addSynopsis.Multiline = true;
            this.addSynopsis.Name = "addSynopsis";
            this.addSynopsis.Size = new System.Drawing.Size(255, 77);
            this.addSynopsis.TabIndex = 8;
            // 
            // labelSynopsis
            // 
            this.labelSynopsis.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSynopsis.Location = new System.Drawing.Point(10, 163);
            this.labelSynopsis.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSynopsis.Name = "labelSynopsis";
            this.labelSynopsis.Size = new System.Drawing.Size(89, 40);
            this.labelSynopsis.TabIndex = 7;
            this.labelSynopsis.Text = "Краткое содержание";
            // 
            // buttonAddNewFilm
            // 
            this.buttonAddNewFilm.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.buttonAddNewFilm.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddNewFilm.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonAddNewFilm.Location = new System.Drawing.Point(136, 264);
            this.buttonAddNewFilm.Name = "buttonAddNewFilm";
            this.buttonAddNewFilm.Size = new System.Drawing.Size(116, 42);
            this.buttonAddNewFilm.TabIndex = 9;
            this.buttonAddNewFilm.Text = "Добавить";
            this.buttonAddNewFilm.UseVisualStyleBackColor = false;
            this.buttonAddNewFilm.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // addFilmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(410, 334);
            this.Controls.Add(this.buttonAddNewFilm);
            this.Controls.Add(this.addSynopsis);
            this.Controls.Add(this.labelSynopsis);
            this.Controls.Add(this.addActors);
            this.Controls.Add(this.labelActors);
            this.Controls.Add(this.addProducer);
            this.Controls.Add(this.labelProducer);
            this.Controls.Add(this.addTitle);
            this.Controls.Add(this.labelTitle);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "addFilmForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление фильма";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.TextBox addTitle;
        private System.Windows.Forms.TextBox addProducer;
        private System.Windows.Forms.Label labelProducer;
        private System.Windows.Forms.TextBox addActors;
        private System.Windows.Forms.Label labelActors;
        private System.Windows.Forms.TextBox addSynopsis;
        private System.Windows.Forms.Label labelSynopsis;
        private System.Windows.Forms.Button buttonAddNewFilm;
    }
}