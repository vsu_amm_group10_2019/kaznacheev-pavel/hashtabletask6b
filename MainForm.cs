﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTableTask6b
{
    public partial class MainForm : Form
    {
        private string FileName { get; set; }
        HashTable LineHash { get; set; } = new HashTable(50);
        public MainForm()
        {
            InitializeComponent();
        }

        private void menuAddButton_Click(object sender, EventArgs e)
        {
            addFilmForm addForm = new addFilmForm(FormState.ADD);
            addForm.ShowDialog();
            if (addForm.ok)
            {
                if (LineHash.Add(addForm.Film))
                {
                    LineHash.ToGrid(TableFilm);
                }
            }
            else
            {
                MessageBox.Show("Не удалось добавить запись");
            }
            
        }
        private void menuEditButton_Click(object sender, EventArgs e)
        {
            addFilmForm search = new addFilmForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string title = search.Film.Title;
                Film film = LineHash.Find(title);
                if (film != null)
                {
                    addFilmForm edit = new addFilmForm(FormState.EDIT, film);
                    edit.ShowDialog();
                    LineHash.Delete(title);
                    if (LineHash.Add(edit.Film))
                    {
                        LineHash.ToGrid(TableFilm);
                    }
                    else
                    {
                        MessageBox.Show("Не удалось изменить запись");
                    }
                }
                else
                {
                    MessageBox.Show("Нет такого фильма в списке");
                }
            }
            else
            {
                MessageBox.Show("Не удалось изменить запись");
            }
        }
        private void menuSearchButton_Click(object sender, EventArgs e)
        {
            addFilmForm search = new addFilmForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string title = search.Film.Title;
                Film film = LineHash.Find(title);
                if (film != null)
                {
                    addFilmForm show = new addFilmForm(FormState.DISPLAY, film);
                    show.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Нет такого фильма в списке");
                }
            }
        }
        private void MenuDeleteButton_Click(object sender, EventArgs e)
        {
            addFilmForm search = new addFilmForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string title = search.Film.Title;
                LineHash.Delete(title);
                LineHash.ToGrid(TableFilm);
            }
        }
        private void menuNewFileButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                DialogResult result = MessageBox.Show(
                    "Сохранить открытый файл?",
                    "Открыт другой файл",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    SaveToFile();
                }

            }
            FileName = "";
            LineHash.Clear();
            LineHash.ToGrid(TableFilm);
        }
        private void menuSaveFileButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            else
            {
                menuSaveAsButton_Click(sender, e);
            }
        }
        private void menuSaveAsButton_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Json File|*.json";
            saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                FileName = saveFileDialog.FileName;
                SaveToFile();
            }
        }
        private void SaveToFile()
        {
            List<Film> films = new List<Film>();

            for (int i = 0; i < LineHash.Size; i++)
            {
                Node<Film> nodeHead = LineHash.Get(i);
                while (nodeHead != null)
                {
                    films.Add(nodeHead.Film);
                    nodeHead = nodeHead.Next;
                }
            }
            string result = JsonConvert.SerializeObject(films);
            File.WriteAllText(FileName, result);
        }
        private void menuOpenFileButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                DialogResult result = MessageBox.Show(
                    "Сохранить открытый файл?",
                    "Открыт другой файл",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    SaveToFile();
                }

            }
            openFileDialog.Filter = "Json File|*.json";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                FileName = openFileDialog.FileName;
                string txt = File.ReadAllText(FileName);
                List<Film> films = JsonConvert.DeserializeObject<List<Film>>(txt);
                LineHash.Clear();
                for (int i = 0; i < films.Size; i++)
                {
                    LineHash.Add(films.Get(i));
                }
                /*foreach (Film film in films)
                {
                    LineHash.Add(film);
                }*/
                LineHash.ToGrid(TableFilm);
            }
        }
        private void menuExitButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void menuHelpButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Внешние цепочки\n" +
                "Задан набор записей следующей структуры: название кинофильма, режиссер, список актеров, краткое содержание.\n" +
                "По заданному названию фильма найти остальные сведения.",
                "Условие задачи");
        }
    }
}
