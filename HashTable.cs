﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace HashTableTask6b
{
    class HashTable
    {
        private List<Film>[] Table { get; }
        public int Size { get; }
        public HashTable(int size)
        {
            Size = size;
            Table = new List<Film>[size];
        }
        public bool Add(Film film)
        {
            int hash = Film.Hash(film.Title);
            int index = hash % Size;
            if (Table[index] == null)
            {
                Table[index] = new List<Film>();
            }

            if (Find(film.Title) != null)
            {
                return false;
            }
            Table[index].Add(film);
            return true;
        }

        public Film Find(string title)
        {
            int hash = Film.Hash(title);
            int index = hash % Size;
            List<Film> cell = Table[index];
            for (int i = 0; i < cell.Size; i++)
            {
                Film elem = cell.Get(i);
                if (elem.Title == title)
                {
                    return elem;
                }
            }
            return null;
        }

        public void Delete(string title)
        {
            int hash = Film.Hash(title);
            int index = hash % Size;
            List<Film> cell = Table[index];
            for (int i = 0; i < cell.Size; i++)
            {
                Film elem = cell.Get(i);
                if (elem.Title == title)
                {
                    cell.Delete(i);
                }
            }
        }

        public void Clear()
        {
            foreach (var list in Table)
            {
                list?.Clear();
            }
        }

        public Node<Film> Get(int index)
        {
            if (Table[index] != null)
            {
                return Table[index].Head;
            }
            else
            {
                return null;
            }
        }
        public void ToGrid(DataGridView grid)
        {
            grid.Rows.Clear();
            grid.RowCount = 0;
            for (int i = 0; i < Size; i++)
            {
                if (Table[i] != null && Table[i].Size > 0)
                {
                    for (int j = 0; j < Table[i].Size; j++)
                    {
                        Film elem = Table[i].Get(j);
                        grid.RowCount++;
                        grid.Rows[grid.RowCount - 1].Cells[0].Value = elem.Title;
                        grid.Rows[grid.RowCount - 1].Cells[1].Value = elem.Producer;
                        grid.Rows[grid.RowCount - 1].Cells[2].Value = elem.Actors;
                        grid.Rows[grid.RowCount - 1].Cells[3].Value = elem.Synopsis;
                    }
                }
            }

        }

    }
}
