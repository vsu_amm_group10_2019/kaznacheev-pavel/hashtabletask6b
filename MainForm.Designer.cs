﻿
namespace HashTableTask6b
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNewFileButton = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenFileButton = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveFileButton = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveAsButton = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExitButton = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAddButton = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditButton = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuDeleteButton = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.TableFilm = new System.Windows.Forms.DataGridView();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Director = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.List_of_actors = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Сontent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TableFilm)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemFile,
            this.MenuItemEdit,
            this.MenuItemSearch,
            this.MenuItemHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(552, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MenuItemFile
            // 
            this.MenuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNewFileButton,
            this.menuOpenFileButton,
            this.menuSaveFileButton,
            this.menuSaveAsButton,
            this.menuExitButton});
            this.MenuItemFile.Name = "MenuItemFile";
            this.MenuItemFile.Size = new System.Drawing.Size(40, 21);
            this.MenuItemFile.Text = "File";
            // 
            // menuNewFileButton
            // 
            this.menuNewFileButton.Name = "menuNewFileButton";
            this.menuNewFileButton.Size = new System.Drawing.Size(180, 22);
            this.menuNewFileButton.Text = "New File";
            this.menuNewFileButton.Click += new System.EventHandler(this.menuNewFileButton_Click);
            // 
            // menuOpenFileButton
            // 
            this.menuOpenFileButton.Name = "menuOpenFileButton";
            this.menuOpenFileButton.Size = new System.Drawing.Size(180, 22);
            this.menuOpenFileButton.Text = "Open File";
            this.menuOpenFileButton.Click += new System.EventHandler(this.menuOpenFileButton_Click);
            // 
            // menuSaveFileButton
            // 
            this.menuSaveFileButton.Name = "menuSaveFileButton";
            this.menuSaveFileButton.Size = new System.Drawing.Size(180, 22);
            this.menuSaveFileButton.Text = "Save File";
            this.menuSaveFileButton.Click += new System.EventHandler(this.menuSaveFileButton_Click);
            // 
            // menuSaveAsButton
            // 
            this.menuSaveAsButton.Name = "menuSaveAsButton";
            this.menuSaveAsButton.Size = new System.Drawing.Size(180, 22);
            this.menuSaveAsButton.Text = "Save As";
            this.menuSaveAsButton.Click += new System.EventHandler(this.menuSaveAsButton_Click);
            // 
            // menuExitButton
            // 
            this.menuExitButton.Name = "menuExitButton";
            this.menuExitButton.Size = new System.Drawing.Size(180, 22);
            this.menuExitButton.Text = "Exit";
            this.menuExitButton.Click += new System.EventHandler(this.menuExitButton_Click);
            // 
            // MenuItemEdit
            // 
            this.MenuItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAddButton,
            this.menuEditButton,
            this.MenuDeleteButton});
            this.MenuItemEdit.Name = "MenuItemEdit";
            this.MenuItemEdit.Size = new System.Drawing.Size(43, 21);
            this.MenuItemEdit.Text = "Edit";
            // 
            // menuAddButton
            // 
            this.menuAddButton.Name = "menuAddButton";
            this.menuAddButton.Size = new System.Drawing.Size(180, 22);
            this.menuAddButton.Text = "Add";
            this.menuAddButton.Click += new System.EventHandler(this.menuAddButton_Click);
            // 
            // menuEditButton
            // 
            this.menuEditButton.Name = "menuEditButton";
            this.menuEditButton.Size = new System.Drawing.Size(180, 22);
            this.menuEditButton.Text = "Edit";
            this.menuEditButton.Click += new System.EventHandler(this.menuEditButton_Click);
            // 
            // MenuDeleteButton
            // 
            this.MenuDeleteButton.Name = "MenuDeleteButton";
            this.MenuDeleteButton.Size = new System.Drawing.Size(180, 22);
            this.MenuDeleteButton.Text = "Delete";
            this.MenuDeleteButton.Click += new System.EventHandler(this.MenuDeleteButton_Click);
            // 
            // MenuItemSearch
            // 
            this.MenuItemSearch.Name = "MenuItemSearch";
            this.MenuItemSearch.Size = new System.Drawing.Size(60, 21);
            this.MenuItemSearch.Text = "Search";
            this.MenuItemSearch.Click += new System.EventHandler(this.menuSearchButton_Click);
            // 
            // MenuItemHelp
            // 
            this.MenuItemHelp.Name = "MenuItemHelp";
            this.MenuItemHelp.Size = new System.Drawing.Size(48, 21);
            this.MenuItemHelp.Text = "Help";
            this.MenuItemHelp.Click += new System.EventHandler(this.menuHelpButton_Click);
            // 
            // TableFilm
            // 
            this.TableFilm.AllowUserToAddRows = false;
            this.TableFilm.AllowUserToDeleteRows = false;
            this.TableFilm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TableFilm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TableFilm.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.Director,
            this.List_of_actors,
            this.Сontent});
            this.TableFilm.Location = new System.Drawing.Point(0, 28);
            this.TableFilm.Name = "TableFilm";
            this.TableFilm.ReadOnly = true;
            this.TableFilm.RowHeadersWidth = 51;
            this.TableFilm.Size = new System.Drawing.Size(552, 281);
            this.TableFilm.TabIndex = 1;
            // 
            // Title
            // 
            this.Title.HeaderText = "Название";
            this.Title.MinimumWidth = 6;
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Width = 125;
            // 
            // Director
            // 
            this.Director.HeaderText = "Режиссер";
            this.Director.MinimumWidth = 6;
            this.Director.Name = "Director";
            this.Director.ReadOnly = true;
            this.Director.Width = 125;
            // 
            // List_of_actors
            // 
            this.List_of_actors.HeaderText = "Актеры";
            this.List_of_actors.MinimumWidth = 6;
            this.List_of_actors.Name = "List_of_actors";
            this.List_of_actors.ReadOnly = true;
            this.List_of_actors.Width = 125;
            // 
            // Сontent
            // 
            this.Сontent.HeaderText = "Содержание";
            this.Сontent.MinimumWidth = 6;
            this.Сontent.Name = "Сontent";
            this.Сontent.ReadOnly = true;
            this.Сontent.Width = 125;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(552, 309);
            this.Controls.Add(this.TableFilm);
            this.Controls.Add(this.menuStrip1);
            this.HelpButton = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Фильмы";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TableFilm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemFile;
        private System.Windows.Forms.ToolStripMenuItem menuNewFileButton;
        private System.Windows.Forms.ToolStripMenuItem menuOpenFileButton;
        private System.Windows.Forms.ToolStripMenuItem menuSaveFileButton;
        private System.Windows.Forms.ToolStripMenuItem menuSaveAsButton;
        private System.Windows.Forms.ToolStripMenuItem menuExitButton;
        private System.Windows.Forms.ToolStripMenuItem MenuItemEdit;
        private System.Windows.Forms.ToolStripMenuItem menuAddButton;
        private System.Windows.Forms.ToolStripMenuItem menuEditButton;
        private System.Windows.Forms.ToolStripMenuItem MenuDeleteButton;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSearch;
        private System.Windows.Forms.DataGridView TableFilm;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem MenuItemHelp;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Director;
        private System.Windows.Forms.DataGridViewTextBoxColumn List_of_actors;
        private System.Windows.Forms.DataGridViewTextBoxColumn Сontent;
    }
}

