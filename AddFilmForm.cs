﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTableTask6b
{
    public partial class addFilmForm : Form
    {
        public Film Film { get; } = new Film();
        private FormState FormState;
        public addFilmForm(FormState formState, Film film = null)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        this.buttonAddNewFilm.Text = "ADD";
                        return;
                    }
                case FormState.EDIT:
                    {
                        this.buttonAddNewFilm.Text = "EDIT";
                        addTitle.ReadOnly = true;
                        addTitle.Text = film.Title;
                        addProducer.Text = film.Producer;
                        addActors.Text = film.Actors;
                        addSynopsis.Text = film.Synopsis;
                        Text = "Изменение";
                        break;
                    }
                case FormState.DELETE:

                case FormState.SEARCH:
                    {
                        this.buttonAddNewFilm.Text = "SEARCH";
                        addProducer.ReadOnly = true;
                        addActors.ReadOnly = true;
                        addSynopsis.ReadOnly = true;
                        Text = "Поиск фильма";
                        break;
                    }

                case FormState.DISPLAY:
                    {
                        addTitle.ReadOnly = true;
                        addTitle.Text = film.Title;
                        addProducer.Text = film.Producer;
                        addSynopsis.Text = film.Synopsis;
                        addActors.Text = film.Actors;

                        addProducer.ReadOnly = true;
                        addActors.ReadOnly = true;
                        addSynopsis.ReadOnly = true;
                        Text = "Фильм";
                        this.buttonAddNewFilm.Text = "OK";
                        break;
                    }

            }
        }
        public bool ok;
        private void btn_add_Click(object sender, EventArgs e)
        {
            if (FormState == FormState.ADD || FormState == FormState.DELETE || FormState == FormState.SEARCH || FormState == FormState.EDIT)
            {
                if (addTitle.Text != "")
                {
                    Film.Title = addTitle.Text;
                    ok = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show(
                            "Название не указано!",
                            "Ошибка ввода!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                }

            }

            if (FormState == FormState.ADD || FormState == FormState.EDIT)
            {
                if (addProducer.Text != "" && addActors.Text != "" && addSynopsis.Text != "" && addTitle.Text != "")
                {
                    Film.Producer = addProducer.Text;
                    Film.Synopsis = addSynopsis.Text;
                    Film.Actors = addActors.Text;
                    ok = true;
                    this.Close();
                }
                else
                {
                    ok = false; //могут ли быть фильмы без сведений?
                    MessageBox.Show(
                              "Заполнены не все поля!",
                              "Ошибка ввода!",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
                }

            }


            if (FormState == FormState.DISPLAY)
            {
                ok = true;
                this.Close();
            }

        }
    }
}
