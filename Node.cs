﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableTask6b
{
    public class Node<T>
    {
        public T Film { get; set; }
        public Node<T> Next { get; set; }
    }
}
