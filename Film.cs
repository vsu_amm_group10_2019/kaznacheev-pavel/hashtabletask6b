﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HashTableTask6b
{
    public class Film
    {
        public string Title { get; set; }
        public string Producer { get; set; }
        public string Actors { get; set; }
        public string Synopsis { get; set; }
        
        public static int Hash(string title)
        {
            int result = 0;
            foreach (char c in title)
            {
                result += c;
            }
            return result;
        }

    }
}
